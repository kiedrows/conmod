#!/usr/bin/bash -l
if [[ $1 -eq "load" ]]; then
	export INITMOD=$2
	bash --init-file /modrc  
else
        source /etc/profile.d/z00_lmod.sh
	module $1 $2 $3 $4
fi
