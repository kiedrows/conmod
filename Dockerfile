FROM centos:7.5.1804
COPY mod.sh /
COPY modrc /
ENV LMOD_VER 7.8.2
ENV LMOD_DIR=/usr/share/lmod/lmod/libexec/
ENV LMOD_COLORIZE=yes
ENV LMOD_SETTARG_CMD=:
ENV LMOD_sys=Linux
ENV LMOD_arch=x86_64
ENV LMOD_PREPEND_BLOCK=normal
ENV LMOD_PKG=/usr/share/lmod/lmod
ENV LMOD_CMD=/usr/share/lmod/lmod/libexec/lmod
ENV LMOD_FULL_SETTARG_SUPPORT=no
#

MAINTAINER Lars Melwyn <melwyn (at) scico.io> et.al. dts@embl.de

RUN  yum -y install epel-release && yum -y update && yum -y install git tar which bzip2 xz \
     bash-completion unzip patch python-keyring lua lua-posix lua-filesystem tcl \
     iproute sudo Lmod &&  yum clean all

RUN useradd -u 1000 -d /home/apps apps && usermod -a -G wheel apps && echo '%wheel ALL=(ALL)       NOPASSWD: ALL'>>/etc/sudoers

USER root
RUN  mkdir -p /g/easybuild && \
     rm -fr /usr/share/modulefiles && \
     ln -s /g/easybuild/x86_64/CentOS/7/skylake/modules/all/ /usr/share/modulefiles 
     #ln -s ${LMODDIR}/software/Lmod/lmod/lmod/init/profile /etc/profile.d/modules.sh  && \
     #ln -s ${LMODDIR}/software/Lmod/lmod/lmod/init/cshrc /etc/profile.d/modules.csh

USER apps 
WORKDIR /home/apps

ENTRYPOINT ["/mod.sh"]
