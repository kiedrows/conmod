#!/bin/bash
#
#set -x
declare -a arr=("avner" "lancrin" "hd-avner" "hd-lancrin" "bioinformatics" "hd-gross" "hd-hackett")
bindsep=" -v"
for share in "${arr[@]}"; do
	mountpoint /mnt/$share &>/dev/null
	bindstr="$bindstr$bindsep /mnt/$share:/g/$share " 
	#
	if [ $? -ge 1 ]; then  
		mount -v /mnt/$share
	fi
done
#
docker run $bindstr -ti conmod:1.0 $1 $2 $3 $4 $5 $6 $7
